# CÉKWA1DEV

Par Philippe Pary, le 15 septembre 2016 pour Pop School Valenciennes http://pop.eu.com  
Licence CC-BY-SA

## À quoi forme-t-on à PopSchool ?

**Au développement web**

*(et on se met doucement à l’IoT)*

###  de la promotion 1

<table>
    <tr>
        <th></th>
        <th>Métier</th>
        <th>Nb élèves</th>
    </tr>
    <tr>
        <td>Web Agency</td>
        <td>Dev web, souvent seul ou en binome, en relation avec une équipe (graphiste, chef de projet etc.)</td>
        <th>3</th>
    </tr>
    <tr>
        <td>Petite SSII</td>
        <td>Dev web en équipe (3-5 personnes) sur des projets plus complexes</td>
        <th>2</th>
    </tr>
    <tr>
        <td>Indépendant</td>
        <td>Dev web freelance</td>
        <th>2</th>
    </tr>
    <tr>
        <td>PME</td>
        <td>Informaticien *homme à tout faire*</td>
        <th>5</th>
    </tr>
    <tr>
        <td>Collectivité</td>
        <td>Réunionite aigue</td>
        <th>1</th>
    </tr>
</table>

## À quoi passe-t-on son temps ?

Estimation purement subjective …

![Graphique très explicite: Coder 40%, Réunion 25%, Veille technologique 15%, Indeterminé 10%](images/ChartGo.png)

## Quelques stats sur le métier

* 4 à 6% de femmes
* Salaires éleveés. Nous constatons de 1250€ à 1500€nets mensuel en promotion 1
* Métier peu ou pas syndiqué, convention collective pourrie (SYNTEC)
* Métier *souple* : pas de dress code sauf dans de rares sociétés, ambiance *start up* généralisée
* Carrières *grenouille* : taux de turn over autour de 30%, on progresse peu par promotion ; essentiellement en changeant de société

## Culture code …

![RTFM !](images/mao.png)

RMLL, FOSDEM, ChtiJS & cie  
Game one (*geek du grand public*) vs NoLife TV (*vrai geek*)

Culture qui évolue avec la généralisation du métier, évidement

# Tout ce que vous avez toujours voulu savoir sur le code, mais n’osiez pas demander

Le code, c’est du texte. Du texte plat, sans accent, sans guillemets. Juste du texte.  
Le développement web, c’est du texte, juste du texte (HTML, CSS, JS), qui est éxecuté par un logiciel, le navigateur web.  
Des logiciels, des serveurs webs, préparent ce texte (partie back) et l’envoient au visiteur (partie front) au travers du protocole HTTP.  
Et pour savoir quel texte-code on doit rédiger, on doit aller lire du texte-documentation

![Lutte contre l’illétrisme](images/illetrisme.jpg)

*page web*
**HTML** est le langage pour structurer les *pages web*. CSS est le langage pour faire le design de ces *pages web*

*serveur web*
**Apache** est un *serveur web*. Il transmet les pages au visiteur

*langage de programmation*
**PHP** ou **Ruby** sont des langages de programmation back, ils permettent de générer des pages web à la volée, à partir d’une base de données ou de formulaires remplis par l’utilisateur.  
**JavaScript** est un langage de programmation front, il permet de rendre les pages web dynamiques, par exemple en chargeant tout seul des nouveautés (cf facebook, tweetdeck etc.)

*framework*
**Symfony** ou **rails** sont des *framework* back, tout un tas de code pré-mâché pour faciliter la vie des développeurs.  
**jQuery** ou **bootstrap** sont des *framework* front, tout un tas de code pré-mâché pour faciliter la vie des développeurs.  

Bon, en fait, il existe des *framework* comme angularJS est à la fois front et back. Mais on va en rester aux choses simples, non ? :)

## HTML simple

Voici un exemple de page web simple, sans mise en page

    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Mon site web</title>
    </head>
    <body>
        <h1>Mon site web est chouette <small>vous ne trouvez pas ?</small></h1>
        <div>Hello world!</div>
    </body>
    </html>

## HTML + CSS

Voici un exemple de page avec mise en page sommaire

    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Mon site web</title>
        <style>
            body {
                font-family: Helvetica, sans;
            }
            h1 small {
                color: grey;
                margin: 0 5px;
                }
        </style>
    </head>
    <body>
        <h1>Mon site web est chouette <small>vous ne trouvez pas ?</small></h1>
        <div>Hello world!</div>
    </body>
    </html>

## HTML + BootStrap

Les frameworks, c’est magique


    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Mon site web</title>
        <link rel="stylesheet" href="bootstrap.min.css">
    </head>
    <body>
        <h1>Mon site web est chouette <small>vous ne trouvez pas ?</small></h1>
        <div>Hello world!</div>
    </body>
    </html>

## Et en JavaScript ? Et en PHP ?

Allons regarder le code d’Ingrid:

<https://framagit.org/lazay/API_watch_2.0>

# Et on fait quoi à POP ?


## Temps consacré …

Ceci est estimatif

<table>
    <tr>
        <th>Technos</th><th>Meta-techno</th><th>Misc</th>
    </tr>
    <tr>
        <td>Back: 150H</td>
        <td>Gestion de projet: 40H</td>
        <td>Veille technologique: 80H</td>
    </tr>
    <tr>
        <td>Front: 150H</td>
        <td>Gestion ordinateur et serveur: 20H</td>
        <td>Création entreprise: 20H</td>
    </tr>
    <tr>
        <td>Dev mobile: 100H</td>
        <td>Sécurité: 20H</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>Droit auteur, vie privée: 10H</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>IDE (outil de développement): 10H</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>400H</th>
        <th>100H</th>
        <th>100H</th>
    </tr>
</table>

## Listing des technos

* HTML5 (langage front)
* CSS3 (langage front)
    * Bootstrap (framework)
* PHP (langage back)
    * Symfony (framework)
* JavaScript (langage front)
    * jQuery (framework)
    * AngularJS (framework, avec un peu de back)
    * iOnic (framework, orienté mobile)
* Ruby (langage back)
    * rails (framework)
* Gnu/Linux (système d’exploitation)
    * VirtualBox (virtualisation)
* Atom (éditeur de texte pour le code)
* Git (gestion de versions)
* Slack/Trello/… (gestion de projet)
* Console de développement des navigateurs web
    * CTRL+option+J dans Safari
    * CTRL+MAJ+J dans Firefox et Chrome

# Parti pris pédagogique

## Rappel

PopSchool est une école d’informatique sans pré-recquis de diplôme.  
**Objectif:** Amener des jeunes vers l’emploi informatique en moins de 6 mois

## Choix

Choix du développement web

Ce choix peut changer en fonction des évolutions des demandes des entreprises

Choix du langage Ruby

PHP est saturé. Ruby manque peut-être d’offres, mais il est plus facile de s’auto-former à PHP qu’à Ruby

## Terre à terre vs étoiles

![Terre à terre vs étoiles](http://www.commitstrip.com/wp-content/uploads/2016/09/Strip-Apprendre-a-etre-dev-650-final-1.jpg)

1. Nous avons fait le choix de passer du temps sur les fondamentaux plutôt que sur les technologies à la mode. HTML, CSS, JS, git … Un manque sur l’alogrythmique. 2 mois consacrés à ces bases
2. Importance donnée à la pratique en entreprise: deux mois en stage, projets en permanence.
3. In fine, nous passons peu de temps dans les frameworks (contrairement au Wagon ou à Simplon)

Hao et Baptiste illustrent à notre sens l’intérêt de notre modèle vis à vis des autres écoles d’informatique en 6 mois.

# conclusion 

![RTFM !](images/mao.png)
