# Installation de VirtualBox

![Logo VirtualBox](https://upload.wikimedia.org/wikipedia/commons/d/d5/Virtualbox_logo.png)

1. Paquets VirtualBox installés
2. Créer une VM (30Go de disque dur pour voir) Debian testing 64bits
3. Virtualisation activée au niveau du processeur (tenter de lancer une VM)

# Installation de Debian

![Logo Debian](https://www.debian.org/logos/openlogo.svg)

1. Pas de partitionnement particulier, pas que ça à foutre
2. Pas d’interface graphique, c’est pour les faibles
3. Pas de serveur d’impression (pourquoi faire sur une VM sérieux ?)
4. Pas de serveur web : ça serait trop facile sinon
5. Serveur SSH, à la limite …

On boote on vérifie qu’on arrive à se logguer comme root (be root, don't reboot !)

## On assaisonne

1. On installe les paquets suivants : openssh-server, sudo, [vim|emacs], htop, multitail, screen
2. On ajoute son utilisateur aux groupes suivants : sudo (pour être sudoer…), adm (pour accéder aux logs) et www-data (pour modifier les fichiers du serveur web)
3. On copie depuis son poste sa clef SSH : $ ssh-copy-id <ipduserveur> (ipduserveur via ifconfig)
4. On tente de se connecter en SSH sur la machine virtuelle : $ ssh <ipduserveur>
5. On tente de se connecter en SFTP sur la machine virtuelle (nautilus, filezilla)
6. On retire la connexion root via SSH (fichier /etc/ssh/sshd_config, PermitRootLogin no), on relance le service (#  service ssh reload)

## On installe le serveur web

1. On installe apache2 : serveur web
2. On installe mysql-server : serveur de base de données
3. On installe php5 : interpréteur de scripts PHP
4. On installe phpmyadmin : client MySQL en PHP

# Tout ceci mérite quelques explications

## Debian
* C’est un environnement de dev, on ne se cassera pas la tête à créer des partitions
* Debian sans interface graphique ne prend pas plus de 10Go. À 30Go, on est larges
* C’est un serveur de dev : l’interface graphique c’est sur votre PC. Le serveur doit servir des pages web, c’est tout. On peut installer un environnement de dev sur sa machine (mais on veut vous apprendre à utiliser un serveur plutôt que votre machine)

## Assaisonnement

* Openssh-server : accès distant
* sudo : sudo, pour lancer des commandes administrateur à la volée
* vim (ou emacs) : pour bricoler à l’arrache des fichiers
* htop : top amélioré
* multitail : pour surveiller plusieurs logs (journaux) à la fois
* screen : pour pouvoir lancer des commandes et ne pas les perdre si on se déconnecte brutalement
* la clef SSH : c’est une bonne pratique. Idéalement, on désactiverait l’accès par mot de passe
* Accès root désactivé : là par contre, ça doit être un réflexe. Pas d’accès root à distance !

## Apache http server

![Logo Apache http server](https://httpd.apache.org/images/httpd_logo_wide_new.png)

Apache2 (Apache http server de son vrai nom) est un serveur web populaire sur Internet (~60% des serveurs webs). Il est connu mais lent et subit régulièrement des failles de sécurité.
Un serveur web reçoit des requêtes HTTP et renvoie des fichiers, notamment HTML.
Apache dispose de modules : dir, userdir, proxy et PHP.

**Apache n’interprête pas les scripts PHP lui-même. Apache est un « passe plats »**

Nginx est un serveur web léger de plus en plus populaire.

## MySQL

![Logo MySQL](https://www.mysql.fr/common/logos/logo-mysql-170x115.png)

MySQL est un serveur de base de données (BDD, ou Database alias DB). Un système de gestion de base de données (SGBD) basé sur SQL (Structured Query Language) vise à simplifier la gestion des données.
MySQL est une technologie populaire. Acquise récemment par Oracle (bouh, Oracle, caca), il existe un fork, MariaDB, qui pourrait remplacer MySQL dans les années à venir si Oracle venait à menacer MySQL.
MySQL est souvent critiqué pour des performances, ses erreurs de calcul (argl) ou son comportement erratique (re-argl)

Un SGDB, c’est plus simple et plus rapide que de tenter de gérer vous même l’enregistrement de vos données dans des fichiers textes. Imaginez gérer une base d’un million d’adresses mail dans un fichier texte.

PostgreSQL est un serveur web robuste, complet et rapide de plus en plus populaire.

## PHP5

![Logo PHP](http://php.net/images/logos/php-med-trans.png)

PHP est un langage de programation scripté (ie non-compilé) orienté web unanimement reconnu comme inefficace, lent, bourré de failles de sécurité, sans cohérence syntaxique, manquant de fonctionnalités basiques … Logiquement, il domine donc les autres langages utilisés webs comme python ou ruby.
PHP génère de l’HTML, du CSS, du JavaScript ou des fichiers à transmettre (images, PDF …). En gros, PHP génère de l’HTML.

Apache, si le module PHP est activé, lance PHP pour interprêter la page et servir le code HTML généré au visiteur. Si le module PHP n’est pas activé, la page PHP est transmise telle quelle, avec le code PHP apparaissant alors aux yeux du visiteur.

## PHPMyAdmin

![Logo PHPMyAdmin](https://wiki.phpmyadmin.net/wiki/images/b/b7/Pma_logo.png)

PHPMyAdmin (PMA pour les intimes) est un client MySQL visuel rédigé en PHP. Il vous permet de manipuler plus aisément vos données que si vous utilisez le client en ligne de commande.
Certaines commandes sont ainsi triviales (use <database>, select * from <table> …) et se font en un seul clic. Limite : si vous abusez de PHPMyAdmin, vous ne progresserez pas en SQL. Vivez PMA comme une assistance pour apprendre le SQL : découvrir « comment on fait », mieux voir où se situe une erreur dans une requête etc.

**Soyez conscients qu’ouvrir un accès à PHPMyAdmin fragilise la sécurité de votre serveur**
