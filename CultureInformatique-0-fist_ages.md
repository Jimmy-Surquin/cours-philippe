# Culture générale informatique

Cours distillé au long cours, Philippe Pary 2016. CC-BY-SA (most recent)

## Objectifs

Ce cours doit introduire aux enjeux du droit d’auteur et du respect de la vie privée. Il présente donc l’informatique, et particulièrement son histoire, en mettant en avant les tensions qui existent autour de la notion de droit d’auteur et du droit à l’intimité et à la vie privée.

Il suit une trame essentiellement historique. La lecture de *hackers* de Steven Levy est chaudement recommandée.

## Partie 1 : la pré-histoire

### Dark ages

L’informatique ne commence qu’avec l’apparition des machines programmables capables de traîter des données. Cependant, cette évolution technique a été rendue possible par plusieurs choses évoquée en vrac ici.

* Pascaline : inventée par Blaise Pascal, première calculette (1642)
* Programmation : Ada Lovelace pose les premiers algorithmes (1840) (fille de Lord Byron)
* Machine de Turing : notion d’ordinateur, par Alan Turing (1936)

On notera également l’existence des tisseuses programmables au XIXe siècle (machine de Jacquart, 1805)

L’objectif est ancien : créer un outil générique qui, sans grand surcoût, puisse être modifié

### Aube

L’informatique voit ses balbuciemments au début du XXème siècle (IBM est fondé en 1911) jusqu’à la seconde guerre mondiale (Alan Turing, contributon d’IBM à l’holocauste)

Les années 40 et 50 voient l’apparition de l’informatique moderne. Les machines sont excessivement coûteuses (plusieurs dizaines de millions d’euro) pour des résultats faible (la moindre montre a plus de puissance que toutes les machines de l’époque réunies) nécessitant un personnel hautement qualifié.

La programmation se fait en assembleur (programmation bas niveau), les premiers langages (LISP, FORTRAN) sont à destination d’un public averti et mathématicien.

L’informatique sert alors essentiellement à l’armée (balistique. Vous croyez que ça servait à quoi d’envoyer des fusées dans l’espace ?) ou aux grandes données économique (assurance, banque, collecte des impôts, statistiques nationales)
Vu le coût d’une machine, le logiciel n’a aucune importance. La question des droits d’auteur sur les logiciels ne se posait pas : le code source était distribué avec les ordinateurs, aucun contrôle n’était effectué sur la diffusion du logiciel. L’échange était la norme.
La question des données personnelles ne se posait pas : l’échange était essentiellement technique et scientifique. Le peu de données personnelles éventuellement présentes étant constitué des premiers jeux (spacewar! 1962) ou de rares poésies (PCC, Californie)

Les financements sont nombreux et généreux. Dans les universités américaines nait une culture informatique (mouvement hacker au MIT)

L’informatique dispose alors d’une image exécrable auprès du grand public (destruction d’ordinateurs lors des manifestations contre la guerre du Viet-Nam, loi *informatique et liberté* de 1976) qui entraîne un premier mouvement de protection du matériel informatique (serrures, vigiles …) qui sera combattu par le mouvement hacker naissant.
