# Publication CV et candidatures de stage

Philippe Pary. PopSchool Valenciennes. pp@pop.eu.com http://pop.eu.com  
CC-BY-SA

## Publier son CV

Bosser sur son ordinateur, c’est bien. Mais ce n’est pas publiquement accessible, ce qui n’est pas pratique pour les éventuels recruteurs.  
Votre CV est la vitrine de votre savoir-faire : consacrez-y du temps

### Choix possibles

* Hébergement sur un serveur dédié ou mutualisé à soi : meilleure solution (on maîtrise son environnement ou le choix de son environnement), mais souvent honéreux (de 2€ à 15€/mois)
* Hébergement chez un hébergeur associatif / sur le site de Pop : solution agréable (on a pas mal de choix, une console d’admin ou la possibilité de discuter avec l’admin sys). C’est gratuit ou à prix libre
* Auto-hébergement : monter un petit serveur à la maison, héberger. Gratuit, peut être mis en avant sur le CV comme compétence supplémentaire, respecte l’esprit d’Internet (décentralisation)
* Github Pages : système de publication de pages web depuis un compte github. Gratuit, génère les pages depuis PHP, Python, Ruby …
* Frama.io : initiative libriste, similaire à Github Pages mais basé sur un compte framagit. Gratuit également mais ne permet de publier que des fichiers HTML. Si le CV est PHP/Ruby/…, il faut générer les pages manuellement

Nous utiliserons **frama.io**

### Frama.io

![RTFM](images/mao.png)

[RTFM](https://frama.io/)

## Postuler

### Intitulés des postes

Vous êtes des développeurs webs :
* Intégrateur HTML
* Web designer
* Développeur front office
* Développeur back office
* Développeur full stack

Selon l’intérêt que vous portez au sujet vous pouvez également faire:
* Administrateur système
    * dev op
* Administrateur base de données (DBA)
* Technicien support

Cette liste est évidement non-exhaustive

### Où chercher ?

Sur le site de Pole Emploi, LOL

#### Sites d’annonces

* [Euratechnologies](http://www.euratechnologies.com/euratechjob/offres)
* Monster, RegionJob, CadrEmploi et autres sites d’emploi sérieux
* [LoLix](http://fr.lolix.org)

#### Maraude

Aller sur les annuaires d’entreprises, visiter leurs sites web. Souvent on y trouve une section emploi.

#### Candidature libre

Vous pouvez envoyer un CV à toute entreprise qui vous semble intéressante

### Que dire ?

**Faites très attention à votre orthographe**

* Que vous cherchez un stage
* Des détails sur le stage : durée, mode, éventuellement rémunération
* Évoquez PopSchool
* Que vous êtes grand débutant
* Personnalisez le mail en montrant que vous vous êtes un minimum intéressé à l’entreprise (rien n’est plus gonflant que d’avoir l’impression que le mail est un copier/coller)

Points d’attention :  
* Inutile de dire votre nom : il apparaît dans le client mail
* Je vous ai dit de faire attention à l’orthographe ?
* Inutile de faire un mail trop long, il ne sera pas lu. Allez droit au but

#### Exemples concrets

(here be exemples concrets)

## Conseils

* Postulez beaucoup
* Tenez une liste des entreprises contactées pour éviter les doublons
* N’ayez pas peur : vous ne serez jamais pris si vous ne postulez pas
* Soyez vous-même, ne vous forcez pas à *faire des phrases*
* Sur votre CV, n’hésitez pas à mettre vos expériences pro même en *job de merde*. Ils prouvent votre capacité à travailler même sur des jobs peu intéressants; votre capacité à vous lever tôt, avoir une hiérarchie pressante, des contraintes de productivité etc.
* Sur votre CV, n’hésitez pas à mettre vos expériences pro même en *job de merde*. Ils prouvent votre capacité à travailler même sur des jobs peu intéressants; votre capacité à vous lever tôt, avoir une hiérarchie pressante, des contraintes de productivité etc.
* Postulez énormément

Votre profil est celui d’un grand débutant, restez humbles sur vos compétences.  
Pour votre intégration, une énorme quantité de travail sera nécessaire. Vous ne pourrez décemment pas vous en tenir à *faire vos heures* sans prendre des risques.  
Les formations comme Pop sont encore très mal connues
