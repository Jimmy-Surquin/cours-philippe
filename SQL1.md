# Cours SQL, partie 1 : introduction

4 juillet 2016, Philippe Pary (pp@pop.eu.com) CC-BY-SA

## Kézako une base de données

Ce sont des logiciels qui vont s’occuper d’enregistrer et de retrouver des données pour vous.  
Ces informations sont structurées (schéma, modèle de données) et on y accède via un langage de requêtes appelé SQL  
Nous travaillerons uniquement avec des *bases de données relationnelles*. Ça vous fait une belle jambe :)

## Structure des données
![oh un lapinou](images/lapinou1.jpeg)

### Serveur

Un serveur contient plusieurs bases. Une base contient un ensemble de données, on peut résumer ainsi : *une base de données == une application*

### Base de données

Une base de données contient plusieurs tables. Les tables sont un set de données (utilisateurs, articles, commentaires …)

### Table

Chaque table est composée d’une série de colonnes (id, nom, prénom, date de naissance, date de dernière connexion …). Chaque colone a un type

Types courants :
* int (nombre)
* varchar 255 (texte court) ou text (texte long)
* date (date)
* blop (binaire, pour les images)

Chaque ligne de la table correspond à un enregistrement

### Illustration

serveur base de données  
     |  
     |- base de données 1 : 100m  
     |          |- table 1 : coureurs  
     |          |       |- id (int)  
     |          |       |- nom (varchar 255)  
     |          |       |- prenom (varchar 255)  
     |          |- table 2 : résultats  
     |                  |- coureurId (int)  
     |                  |- temps (int)  
     |  
     |- base de données 2 : PopSchool  
     |          |- table 1 : promotions  
     |          |       |- id (int)  
     |          |       |- nom (varchar 255)  
     |          |       |- datedébut (date)  
     |          |       |- datefin (date)  
     |          |- table 2 : élèves  
     |          |       |- id (int)  
     |          |       |- nom (varchar 255)  
     |          |       |- promotionId (int)  
     |          |       |- datedenaissance (date)  
     |          |- table 3 : attestations  
     |                  |- eleveId (int)  
     |                  |- attestation (int. 0: non, 1: oui)  
     |  
     |- base de données 3 : annuaire de jeux  
                |- table 1 : constructeurs  
                |       |- id (int)  
                |       |- nom (varchar 255)  
                |- table 2 : consoles  
                |       |- id (int)  
                |       |- constructeurId (int)  
                |       |- nom (varchar 255)  
                |- table 3 : jeux  
                |       |- id (int)  
                |       |- nom (varchar 255)  
                |- table 4 : consolesJeu  
                        |- jeuId (int)  
                        |- consoleId (int)  

### Exercice
![oh un autre lapinou](images/lapinou2.jpeg)

Nous allons jeter les bases d’un projet au long cours : un jeu de pendu  
Nous avons besoin de 3 tables : une pour avoir les mots, une pour les joueurs et une pour compter le nombre d’essais

#### Stocker ou compter ?

Il existe un dilemme éternel en SQL : stocker ou compter ?  
Doit-on stocker dans une base à part le meilleur score pour un mot ?

* *Pour* l’information est plus rapide et plus facile à stocker
* *Contre* on créera une table inutile, on risque les *race condition* ou *situation de concurrence*. 

Il se passe quoi si le score minimal stocké est différent du score minimal constaté dans la base des essais ? Ce genre de choses arrivent via des bugs ou des sa race de race condition (deux enregistrement d’un même meilleur score dans un cours laps de temps)

Exemple de race condition:  
Supposons un programme mal pensé
1. Aline commence une partie, le programme récupère le meilleur score (5)
2. Samuel commence une partie, le programme récupère le meilleur score (5)
3. Aline a terminé, son score de 3 est enregistré dans les essais et dans la table des meilleurs score
4. Samuel a terminé, son score de 4 est enregistré dans les essais et dans la table des meilleurs score

Nous avons donc 3 essais : 5, 3 et 4. Cependant, la table du meilleur score contient 4 comme record

#### Mise en œuvre

Via l’interface visuelle de PHPMyAdmin

1. Se connecter à PHPMyAdmin
2. Créer une base de données *penduApp*
3. Créer une table *mots* avec pour champs *id* (int, **AI**), *mot* (varchar 255)
4. Créer une table *joueurs* avec pour champs *id* (int **AI**), *nom* (varchar 255)
5. Créer une table *compteurs* avec pour champs *motId* (int), *joueurId* (int), *essais* (int), *trouve* (int, 0 ou 1)

**AI** est *auto-increment*, une case à cocher à la création. Le champ sera une clef primaire, c’est à dire que chaque valeur sera unique et servira à identifier chaque ligne

## Les 4 requêtes de base
![Oh Yeah !](images/lapinou3.jpeg)

SQL dispose de 4 requêtes de base
* INSERT
* SELECT
* UPDATE
* DELETE
Aussi intelligement appelés *CRUD* (Create, Read, Update, Delete) …

Ces dernières fonctionnent partout : MySQL, PostgeSQL, H2DB & cie

Obtenez un mémo SQL (livret physique, fichier PDF, page web) pour connaître la syntaxe des requêtes.  
PHPMyAdmin affiche toujours la requête de l’opération que vous réalisez, vous pouvez vous en servir pour découvrir comment rédiger une requête.

### INSERT

    INSERT INTO mots (‘mot‘) VALUES ('tartiflette');
    INSERT INTO mots (‘mot‘) VALUES ('trotinette'),('éléphant'),('livre'),('tomate');

INSERT crée une nouvelle ligne dans une table de données  
Il est inutile d’indiquer les champs auto-incrémentaux, ils ont une valeur automatiquement attribuée

### SELECT

    SELECT * FROM mots;
    SELECT * FROM mots WHERE id > 3;

SELECT récupère des informations à partir d’une table de données

### UPDATE

    UPDATE mots SET ‘mot‘='cornichon' WHERE id=1;

UPDATE met à jour une ligne dans une table de données

### DELETE

    DELETE FROM mots WHERE id=2;

DELETE supprime une ligne dans une table de données

## Exercices
![Kawaïïïïï](images/lapinou4.jpeg)

### INSERT

1. Créer 20 mots
    1. pomme
    2. bateau
    3. poire
    4. éléphant
    5. tartiflette
    6. cornichon
    7. trotinette
    8. livre
    9. tomate
    10. vélo
    11. paquebot
    12. chyropracteur
    13. tractopelle
    14. huche
    15. cacahuète
    16. livraison
    17. circonstance
    18. vaporisateur
    19. chaise
    20. chèvre
2. Créer 5 joueurs
    1. vous
    2. Aline
    3. Samuel
    4. Delphine
    5. Aziz

### SELECT

1. Sélectionner tous les mots
2. Sélectionner tous les utilisateurs
3. Sélectionner les mots qui commencent par la lettre c (WHERE mot LIKE 'c%')
4. Sélectionner tous les mots dont l’id est inférieur à 5

### UPDATE

1. Remplacer huche par buche
2. Remplacer le 5eme mot par choucroute
3. Remplacer Aline par Lætitia

### DELETE

1. Effacer le mot buche
2. Effacer tous les mots dont l’id est supérieur à 15

## Exercices libres

Créer une ou plusieurs des bases de données évoquées. Insérer des données fictives, jouer avec.

Vous pouvez aussi essayer de penser votre propre schéma de données ; consultez impérativement un formateur si vous vous lancez dans l’aventure. Je n’ai aucun doute que vous allez échouer à produire un schéma cohérent

## Lexique

* **SQL** structured query language
* **BDD** base de données
* **DB** database
* **SGBD** système de gestion de base de données
* **DBMS** database management system
* **DBA** database administrator

![Mouuuuuuuhhhhh](images/lapinou5.jpeg)
